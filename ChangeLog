commit 42d5a96fc6cd69f3b787b5030386e0bcbb9b0cab
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Jun 28 11:32:14 2011 +1000

    mutouch 1.3.0
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit 7efc947e3cb2fe254f5b44bd1a0f4629fb777a95
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Jun 28 00:57:57 2011 +1000

    Don't reset the stored device's private
    
    Storing references to the finger/stylus pInfo is dangerous as-is,
    but accessing them during the UnInit is suboptimal. This pInfo may have
    been freed already.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit d23472b709fafd48f8403e6ea97918c06cc3caaf
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Jun 28 00:45:11 2011 +1000

    Don't free pInfo in Uninit.
    
    Just free priv, reset the pointer to NULL and let the server do the
    rest.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit 9aaaf67f5e0bb7419c24df041a75bb93e2f7c601
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Jun 28 00:43:18 2011 +1000

    Don't call DEVICE_OFF during Uninit
    
    The server will do it for us before anyway.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit 041959f3fcdc473cb92437227522b2e986a1055e
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Jun 28 00:38:38 2011 +1000

    Don't free pInfo on PreInit failure.
    
    The server does it for us. Make sure we reset the private pointer to
    NULL though.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit ea50c9d3a593d6dd2a45a12b39fe9c248ea25309
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Jun 28 00:37:45 2011 +1000

    Require server 1.10 instead of manual ABI checks.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit 29c3cb19938df97fd4a968ac010f48a2e25a3b72
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Jun 28 00:35:13 2011 +1000

    s/MicroTouch/MuTouch
    
    I don't know what the difference is but we had a microtouch driver.
    Having this driver spit out MicroTouch error messages is suboptimal.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit 7de3fd29eaa2f7532658f706ee0471dde34f16d6
Author: Julien Cristau <jcristau@debian.org>
Date:   Sat Apr 17 16:07:29 2010 +0200

    Don't clobber CFLAGS
    
    CFLAGS is a user variable, don't set it from configure.

commit 24029451c591d324fc6e4a64d0bba70841bb1725
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Fri Dec 3 09:07:38 2010 +1000

    Support input ABI 12
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit f7c4418ae0313b35d528fe28cfaf7899bc96e029
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Fri Dec 3 09:06:16 2010 +1000

    Replace use of private_flags with driver-internal device_type.
    
    private_flags is removed in input ABI 12 but it's not needed anyway.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 3390adfb0c4cec5914a1c4e05bb95a6140fe6d31
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:42:33 2010 +1000

    Drop driver-specific motion history size handling.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 9c887eceeb7f6bb1f9005bc2aeedeaa0a5d77ec4
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:42:11 2010 +1000

    Drop close_proc, conversion_proc, reverse_conversion_proc
    
    All three are not called by the server anymore.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 69ea6552035603f1813c80caa14cd8fba2778102
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:38:46 2010 +1000

    Drop libc wrappers for free, malloc
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 287bbb905a28d8f9bcebacb7826f4902e438ec64
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:37:15 2010 +1000

    Replace LocalDevicePtr with InputInfoPtr.
    
    LocalDevicePtr has been dropped from the server, but both describe the same
    struct anyway.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 40920147a87d7f069cf4406f1f50aed58a7ce7e0
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:34:12 2010 +1000

    Require server 1.9, drop earlier ABI support
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 7ad420be74fec993f98fdfe746f037d803a6b927
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:33:33 2010 +1000

    Drop unused bits from configure.ac
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit dae0cddd64bc4ac01f47b8e1d0f298ca42c11382
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:32:55 2010 +1000

    Drop ref count, removed from server.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 75eb5c3b9c70ac9d8c31a762ba9a1ec1dd2485f4
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:32:45 2010 +1000

    unifdef XFree86LOADER
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Cyril Brulebois <kibi@debian.org>

commit 3443b6c6e5ea9f0c709ef865f1bf08ba68e66d67
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:30:22 2010 +1000

    Purge CVS tags
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit d0c22e3c02c25c9bef103f745c2648fb92ec3fa9
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Thu Dec 2 15:30:04 2010 +1000

    Bump to 1.2.99
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit db04a5333b545a0442b995bae47d2c5527a7459e
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Fri Jul 17 14:18:51 2009 +1000

    Cope with XINPUT ABI 7.
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit ec133cb3d485328290b5cdcc6c2817d78f311a5d
Author: Alan Coopersmith <alan.coopersmith@sun.com>
Date:   Fri Jan 30 20:28:11 2009 -0800

    Add README with pointers to mailing list, bugzilla & git repos

commit 301153d4c833590c8f5a19b9f0780c8426abb2d0
Author: Fernando Vicente <fvicente@gmail.com>
Date:   Fri Jan 16 06:53:22 2009 +0100

    Fix calculation of coordinates with inverted axes
    
    Signed-off-by: Julien Cristau <jcristau@debian.org>
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>

commit b988e2ec8d7421f531f9d37f6bdb9653660c038d
Author: Alan Coopersmith <alan.coopersmith@sun.com>
Date:   Fri Jan 9 16:21:31 2009 -0800

    Remove xorgconfig & xorgcfg from See Also list in man page

commit 3a210acc671a6a2f07b2321cf4d7d525867da145
Author: Peter Hutterer <peter.hutterer@redhat.com>
Date:   Thu Oct 2 09:10:15 2008 +0930

    mutouch 1.2.1

commit 226adbe8a87266115eb66a480896d33405124acb
Author: Peter Hutterer <peter.hutterer@redhat.com>
Date:   Thu Aug 14 15:44:27 2008 +0930

    Remove pre-XFREE86_V4 cruft.

commit c2a2c40506a57aa6d9e92e6dce372165c132eb85
Author: Peter Hutterer <peter@cs.unisa.edu.au>
Date:   Sat Jun 21 23:15:14 2008 +0930

    Fix stupid typos from last patch.
    
    Don't mis-use a boolean as temporary variable either.

commit 7ada6945af8c690e629bfc4ad6fe19b49cbbb66e
Author: Peter Hutterer <peter@cs.unisa.edu.au>
Date:   Thu Jun 12 21:27:32 2008 +0930

    Handle axis inversion in the driver.
    
    The current X server doesn't handle inverted axes, so we need to do it in the
    driver. Report valid axis ranges to the server, but flip the coordinates
    before posting events.
    
    Untested for lack of device.

commit aa2cd369f4966520ee90c57035f760b1ca5d9216
Author: Peter Hutterer <peter@cs.unisa.edu.au>
Date:   Thu Jun 12 21:15:28 2008 +0930

    Remove trailing whitespaces.

commit 4c3420534d0f62e37ba251c68ac65e88f9c5b9b9
Author: Peter Hutterer <peter@cs.unisa.edu.au>
Date:   Mon May 26 22:15:35 2008 +0930

    Check for XINPUT ABI 3.

commit 2ec700990edd4bd77973de16ab2b40e754dee7f1
Author: Adam Jackson <ajax@redhat.com>
Date:   Thu Mar 20 16:31:11 2008 -0400

    mutouch 1.2.0

commit 6dbf50f8003fd74697273525bb966e976994e868
Author: Matthieu Herrb <matthieu@bluenote.herrb.net>
Date:   Sat Mar 8 23:12:20 2008 +0100

    Makefile.am: nuke RCS Id

commit 5e10ff7ecda4df10b6e4d8b7767f5fc64923653e
Author: James Cloos <cloos@jhcloos.com>
Date:   Mon Sep 3 05:52:18 2007 -0400

    Add *~ to .gitignore to skip patch/emacs droppings

commit d21723d63ad3ba50baac2a2c67839ca4f75d2779
Author: James Cloos <cloos@jhcloos.com>
Date:   Thu Aug 23 19:25:34 2007 -0400

    Rename .cvsignore to .gitignore

commit 441b82685e44968ed023b553f2dd4282db4db27c
Author: Brice Goglin <bgoglin@debian.org>
Date:   Tue Aug 7 09:39:23 2007 +0200

    Use PACKAGE_VERSION_MAJOR/MINOR/PATCHLEVEL in version_rec

commit e733e8f969cd566bd2ece3afb738e0311fcddcb5
Author: Adam Jackson <ajax@nwnk.net>
Date:   Fri Apr 7 18:02:19 2006 +0000

    Unlibcwrap. Bump server version requirement. Bump to 1.1.0.

commit ae2cc1656600d1be2237e822efc07a9d927c07f2
Author: Kevin E Martin <kem@kem.org>
Date:   Wed Dec 21 02:29:58 2005 +0000

    Update package version for X11R7 release.

commit e8c8388dbf5ed483779d3ce52be6dd873f91c52d
Author: Adam Jackson <ajax@nwnk.net>
Date:   Mon Dec 19 16:25:51 2005 +0000

    Stub COPYING files

commit b1f790f94888d4f8011bd1b0e54bbe602410e0a7
Author: Kevin E Martin <kem@kem.org>
Date:   Thu Dec 15 00:24:15 2005 +0000

    Update package version number for final X11R7 release candidate.

commit 785048e920a8f6d8db615333e7b75587a2dfe06d
Author: Kevin E Martin <kem@kem.org>
Date:   Tue Dec 6 22:48:31 2005 +0000

    Change *man_SOURCES ==> *man_PRE to fix autotools warnings.

commit c48f7b4eaa3202ee06767b6f39ae8a6c90f4e0a0
Author: Kevin E Martin <kem@kem.org>
Date:   Sat Dec 3 05:49:31 2005 +0000

    Update package version number for X11R7 RC3 release.

commit 764ced9de098668041eece797ffd004ee0466002
Author: Kevin E Martin <kem@kem.org>
Date:   Fri Dec 2 02:16:06 2005 +0000

    Remove extraneous AC_MSG_RESULT.

commit 7949be851c9f50edc0d9b181a11c32964ce282cd
Author: Adam Jackson <ajax@nwnk.net>
Date:   Tue Nov 29 23:29:56 2005 +0000

    Only build dlloader modules by default.

commit 0586f0e844901504da5b283756e1aa63a0ed341e
Author: Alan Coopersmith <Alan.Coopersmith@sun.com>
Date:   Mon Nov 28 22:04:07 2005 +0000

    Change *mandir targets to use new *_MAN_DIR variables set by xorg-macros.m4
        update to fix bug #5167 (Linux prefers *.1x man pages in man1 subdir)

commit 9d5518d9ab351b0e8a949fed264fb3f370685edf
Author: Eric Anholt <anholt@freebsd.org>
Date:   Mon Nov 21 10:49:07 2005 +0000

    Add .cvsignores for drivers.

commit 7f7d5e7e7995ec3ebff57e6fb4b8362b314bf1a7
Author: Kevin E Martin <kem@kem.org>
Date:   Wed Nov 9 21:15:12 2005 +0000

    Update package version number for X11R7 RC2 release.

commit b7b562c7fa2f695b978e6ed6764d64230dd0da91
Author: Kevin E Martin <kem@kem.org>
Date:   Tue Nov 1 15:08:51 2005 +0000

    Update pkgcheck depedencies to work with separate build roots.

commit 9a0c829b826a485e397ef00032a812c8a0e639ae
Author: Kevin E Martin <kem@kem.org>
Date:   Wed Oct 19 02:48:01 2005 +0000

    Update package version number for RC1 release.

commit db594b1ecb57e45a48da64244e9f7beb674a4b84
Author: Alan Coopersmith <Alan.Coopersmith@sun.com>
Date:   Tue Oct 18 00:01:52 2005 +0000

    Use @DRIVER_MAN_SUFFIX@ instead of $(DRIVER_MAN_SUFFIX) in macro
        substitutions to work better with BSD make

commit 33fa6668eebed44cbe97ea6179c39f4ea060cce9
Author: Adam Jackson <ajax@nwnk.net>
Date:   Mon Oct 17 22:57:29 2005 +0000

    More 1.7 braindamage: define EXTRA_DIST in terms of @DRIVER_NAME@ instead
        of indirectly

commit 1ea02bdc3c9557f6b5e1506168d35af9fa6eea89
Author: Alan Coopersmith <Alan.Coopersmith@sun.com>
Date:   Mon Oct 17 00:08:59 2005 +0000

    Use sed & cpp to substitute variables in driver man pages

commit 0213d5e01d8a366a4e6412c8885581df09953cca
Author: Daniel Stone <daniel@fooishbar.org>
Date:   Thu Aug 18 09:03:40 2005 +0000

    Update autogen.sh to one that does objdir != srcdir.

commit dd57104958bc29b3ea0487eb9a07cc5747e0799e
Author: Søren Sandmann Pedersen <sandmann@daimi.au.dk>
Date:   Wed Aug 10 14:07:23 2005 +0000

    Don\'t lose existing CFLAGS in all the input drivers and some of the video
        drivers

commit b508bcd4fb19061fd9bd886c293679b246afea59
Author: Kevin E Martin <kem@kem.org>
Date:   Fri Jul 29 21:22:41 2005 +0000

    Various changes preparing packages for RC0:
    - Verify and update package version numbers as needed
    - Implement versioning scheme
    - Change bug address to point to bugzilla bug entry form
    - Disable loadable i18n in libX11 by default (use --enable-loadable-i18n to
        reenable it)
    - Fix makedepend to use pkgconfig and pass distcheck
    - Update build script to build macros first
    - Update modular Xorg version

commit 869e0e0304a841a60fca13ab142a5e7afd6fab26
Author: Kevin E Martin <kem@kem.org>
Date:   Wed Jul 13 20:03:22 2005 +0000

    Make the module dir configurable

commit 1af0ed35da050eb6cfd672ebcf4a244e2ac28f52
Author: Kevin E Martin <kem@kem.org>
Date:   Wed Jul 13 02:21:01 2005 +0000

    Update all input drivers to pass distcheck

commit f2ceee068d4701362608c7de7c69a04dd5a188b9
Author: Adam Jackson <ajax@nwnk.net>
Date:   Tue Jul 12 06:15:09 2005 +0000

    Build skeletons for input drivers. Should basically work.

commit 07fe4780c2d7d56fea95a43732fbec9a3a8553dd
Author: Adam Jackson <ajax@nwnk.net>
Date:   Mon Jul 11 02:38:00 2005 +0000

    Prep input drivers for modularizing by adding guarded #include "config.h"

commit cbf465fc48ee205ee7140eefd63e85b01d3f8f99
Author: Daniel Stone <daniel@fooishbar.org>
Date:   Sun Jul 3 08:53:45 2005 +0000

    Change <X11/misc.h> and <X11/os.h> to "misc.h" and "os.h".

commit 8b67d9612876960d81408bc69631f166ed9e8408
Author: Daniel Stone <daniel@fooishbar.org>
Date:   Fri Jul 1 22:43:23 2005 +0000

    Change all misc.h and os.h references to <X11/foo.h>.

commit 7f550161e4b09e19c207c6b7877182ce11eb9e96
Author: Adam Jackson <ajax@nwnk.net>
Date:   Mon Jun 27 18:32:54 2005 +0000

    Various input drivers set their InputDriverRec to be static when doing a
        loadable build, and the same symbol can't be both static and _X_EXPORT.
        Pointed out by Alan Coopersmith.

commit 51435d524c32daa50a6fdba45b5499e23bacbd1b
Author: Adam Jackson <ajax@nwnk.net>
Date:   Sat Jun 25 21:17:02 2005 +0000

    Bug #3626: _X_EXPORT tags for video and input drivers.

commit bbd906d00f7c5235422133c9935d37eedb87a8e6
Author: Daniel Stone <daniel@fooishbar.org>
Date:   Wed Apr 20 12:25:34 2005 +0000

    Fix includes right throughout the Xserver tree:
    change "foo.h" to <X11/foo.h> for core headers, e.g. X.h, Xpoll.h;
    change "foo.h", "extensions/foo.h" and "X11/foo.h" to
        <X11/extensions/foo.h> for extension headers, e.g. Xv.h;
    change "foo.[ch]" to <X11/Xtrans/foo.[ch]> for Xtrans files.

commit 490a14af12143092496cc2fa032bdd63fe3e2ae9
Author: Egbert Eich <eich@suse.de>
Date:   Fri Apr 23 19:54:04 2004 +0000

    Merging XORG-CURRENT into trunk

commit 3c3fa4c5b9aadb7dc6ec9004d457f76105abfed4
Author: Egbert Eich <eich@suse.de>
Date:   Sun Mar 14 08:33:52 2004 +0000

    Importing vendor version xf86-4_4_99_1 on Sun Mar 14 00:26:39 PST 2004

commit 74981d89fc9d1830d53855db97782bd9a815a69a
Author: Egbert Eich <eich@suse.de>
Date:   Wed Mar 3 12:12:34 2004 +0000

    Importing vendor version xf86-4_4_0 on Wed Mar 3 04:09:24 PST 2004

commit 6c36f26568ed8699c173bc3a3ecd3280748f3614
Author: Egbert Eich <eich@suse.de>
Date:   Thu Feb 26 13:36:00 2004 +0000

    readding XFree86's cvs IDs

commit 931f7cebb38d0ed672021e0911bc269f0591ce85
Author: Egbert Eich <eich@suse.de>
Date:   Thu Feb 26 09:23:34 2004 +0000

    Importing vendor version xf86-4_3_99_903 on Wed Feb 26 01:21:00 PST 2004

commit 00eb3aac7d4d3f17a381ae4fca42baa939f163a0
Author: Kaleb Keithley <kaleb@freedesktop.org>
Date:   Fri Nov 14 16:48:56 2003 +0000

    XFree86 4.3.0.1

commit 7d3ee7b318425801939c01c9ff8850d5821bc224
Author: Kaleb Keithley <kaleb@freedesktop.org>
Date:   Fri Nov 14 16:48:56 2003 +0000

    Initial revision
